<!-- partial:partials/_navbar.html -->
<nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex align-items-top flex-row">
    <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-start" style="background-color: #3b5998">
        <div class="me-3">
            <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-bs-toggle="minimize">
            <span class="icon-menu"></span>
            </button>
        </div>
        <div>
            <a class="navbar-brand brand-logo" href="/">
                <img src="/img/logo.png" alt="">
            </a>
            <a class="navbar-brand brand-logo-mini mr-2" href="/">
                <img src="/img/logo.png" alt="">
            </a>
        </div>
    </div>
    <div class="navbar-menu-wrapper d-flex align-items-top" style="border-bottom: 1px solid #3b5998; background: #3b5998; color: #fff"> 
    <ul class="navbar-nav">
        <li class="nav-item font-weight-semibold d-none d-lg-block ms-0">
        <h1 class="welcome-text">Xin chào, <span class="text-black fw-bold">{{Auth::user()->name}}</span></h1>
        </li>
    </ul>
    <div class="battery ml-2">
        <div id="battery">
           
        </div>
    </div>
    <ul class="navbar-nav ms-auto">
        <li class="nav-item dropdown d-lg-block user-dropdown">
        <a class="nav-link" id="UserDropdown" href="#" data-bs-toggle="dropdown" aria-expanded="false">
            <img class="img-xs rounded-circle" src="/template/admin/images/faces/face8.jpg" alt="Profile image"> </a>
        <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
            <div class="dropdown-header text-center">
            <img class="img-md rounded-circle" src="/template/admin/images/faces/face8.jpg" alt="Profile image">
            <p class="mb-1 mt-3 font-weight-semibold">{{Auth::user()->name}}</p>
            <p class="fw-light text-muted mb-0">{{Auth::user()->email}}</p>
            </div>
            <a class="dropdown-item" href="/logout"><i class="dropdown-item-icon mdi mdi-power text-primary me-2"></i>Đăng Xuất</a>
        </div>
        </li>
    </ul>
    <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-bs-toggle="offcanvas">
        <span class="mdi mdi-menu"></span>
    </button>
    </div>
</nav>

<style>
    .battery {
        display: flex;
        justify-content: center;
        align-items: center;
        background-color: #fff;
        padding: 10px;
    }
</style>

<script>
    var status = 2;
    if (status == 1) {
        document.getElementById("battery").innerHTML = "<i class='mdi mdi-battery-charging-60 text-success'></i>";
        alert('charging');
    } else if (status == 2){
        document.getElementById("battery").innerHTML = "<i class='mdi mdi-battery text-success'></i>";
        alert('Full');
    } else {
        document.getElementById("battery").innerHTML = "<i class='mdi mdi-battery-10 text-danger'></i>";
        alert('Battery low');
    }
</script>
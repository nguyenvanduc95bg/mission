<div id="right-sidebar" class="settings-panel">
    <i class="settings-close ti-close"></i>
    <ul class="nav nav-tabs border-top" id="setting-panel" role="tablist">
        <li class="nav-item">
        <a class="nav-link active" id="todo-tab" data-bs-toggle="tab" href="#todo-section" role="tab" aria-controls="todo-section" aria-expanded="true">TO DO LIST</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" id="chats-tab" data-bs-toggle="tab" href="#chats-section" role="tab" aria-controls="chats-section">CHATS</a>
        </li>
    </ul>
    <div class="tab-content" id="setting-content">
        <div class="tab-pane fade show active scroll-wrapper" id="todo-section" role="tabpanel" aria-labelledby="todo-section">
        <div class="add-items d-flex px-3 mb-0">
            <form class="form w-100">
            <div class="form-group d-flex">
                <input type="text" class="form-control todo-list-input" placeholder="Add To-do">
                <button type="submit" class="add btn btn-primary todo-list-add-btn" id="add-task">Add</button>
            </div>
            </form>
        </div>
        <div class="list-wrapper px-3">
        </div>
        </div>
        <!-- chat tab ends -->
    </div>
    </div>
    <!-- partial -->
    <!-- partial:partials/_sidebar.html -->
    <nav class="sidebar sidebar-offcanvas" id="sidebar" style="background-color: #3b5998">
    <ul class="nav" style="background-color: #3b5998">
        <li class="nav-item">
        <a class="nav-link" href="/">
            <i class="mdi mdi-grid-large menu-icon"></i>
            <span class="menu-title">Trang chủ</span>
        </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-bs-toggle="collapse" href="#tables" aria-expanded="false" aria-controls="tables">
              <i class="menu-icon mdi mdi-arrow-right-bold-hexagon-outline"></i>
              <span class="menu-title">Mission</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="tables">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="/missions">Start & Create mission</a></li>
                <li class="nav-item"> <a class="nav-link" href="#">Mission Editor</a></li>
                <li class="nav-item"> <a class="nav-link" href="#">Mission Action</a></li>
              </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-bs-toggle="collapse" href="#maps" aria-expanded="false" aria-controls="tables">
              <i class="menu-icon mdi mdi-google-maps"></i>
              <span class="menu-title">Map</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="maps">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="/maps">Import & Export site</a></li>
                <li class="nav-item"> <a class="nav-link" target="_blank" href="/create-map">Create & Edit map</a></li>
                <li class="nav-item"> <a class="nav-link" href="#">Mapping tool</a></li>
                <li class="nav-item"> <a class="nav-link" href="#">Recording a map</a></li>
              </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/sound">
                <i class="menu-icon mdi mdi-bookmark-music"></i>
                <span class="menu-title">Sound</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/status">
                <i class="menu-icon mdi mdi-bookmark-music"></i>
                <span class="menu-title">Status</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <i class="menu-icon mdi mdi-autorenew"></i>
                <span class="menu-title">Transition</span>
            </a>
        </li>
        @if(Auth::user()->level == 999)
        <li class="nav-item">
            <a class="nav-link" href="/users">
                <i class="menu-icon mdi mdi-account-outline"></i>
                <span class="menu-title">Users</span>
            </a>
        </li>
        @endif
        <li class="nav-item">
            <a class="nav-link" href="#">
                <i class="menu-icon mdi mdi-account-multiple"></i>
                <span class="menu-title">User groups</span>
            </a>
        </li>
    </ul>
</nav>

<style>
    .sidebar .nav .nav-item .nav-link {
        color: #fff;
        font-weight: bold;
        padding: 15px 35px 15px 35px;
        border-radius: unset;
    }
    .sidebar .nav .nav-item .nav-link i.menu-icon {
        color: #fff;
    }
    .sidebar .nav .nav-item.active > .nav-link i, .sidebar .nav .nav-item.active > .nav-link .menu-title, .sidebar .nav .nav-item.active > .nav-link .menu-arrow {
        color: #1F3BB3;
    }
    .sidebar .nav .nav-item.active > .nav-link i.menu-arrow::before {
        content: "\F142";
    }
</style>
    <!-- partial -->
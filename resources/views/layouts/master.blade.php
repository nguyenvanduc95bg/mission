<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title> MISSION </title>
<base href="{{asset('')}}">
<link rel="icon" type="image/x-icon" href="/img/favicon.png">
  <!-- plugins:css -->
  <link rel="stylesheet" href="/template/admin/vendors/feather/feather.css">
  <link rel="stylesheet" href="/template/admin/vendors/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="/template/admin/vendors/ti-icons/css/themify-icons.css">
  <link rel="stylesheet" href="/template/admin/vendors/typicons/typicons.css">
  <link rel="stylesheet" href="/template/admin/vendors/simple-line-icons/css/simple-line-icons.css">
  <link rel="stylesheet" href="/template/admin/vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="/template/admin/css/style.css">
  <link rel="stylesheet" href="/template/admin/css/vertical-layout-light/style.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
</head>
<body>
  <div class="container-scroller"> 
		@include('layouts.header')
    <!-- partial -->
    <div class="container-fluid page-body-wrapper" style="padding-right: 0; padding-left: 0">
      @include('layouts.sidebar')
      <div class="main-panel">
        <div class="content-wrapper">
            @yield('content')
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        @include('layouts.footer')
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <div class="loader" id="loading"></div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="/template/admin/vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page -->
  <script src="/template/admin/vendors/chart.js/Chart.min.js"></script>
  <script src="/template/admin/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
  <script src="/template/admin/vendors/progressbar.js/progressbar.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"  crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" crossorigin="anonymous"></script>
  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="/template/admin/js/off-canvas.js"></script>
  <script src="/template/admin/js/hoverable-collapse.js"></script>
  <script src="/template/admin/js/template.js"></script>
  <script src="/template/admin/js/settings.js"></script>
  <script src="/template/admin/js/todolist.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="/template/admin/js/dashboard.js"></script>
  <script src="/template/admin/js/Chart.roundedBarCharts.js"></script>
  <!-- DataTables JavaScript -->
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>

  <!-- CK editor -->
  {{-- <script type="text/javascript" language="javascript" src="template/vendors/ckeditor/ckeditor.js" ></script> --}}
  <script>
      $(document).ready(function() {
          $('#dataTables-example').DataTable({
            "ordering": false,
            "info":     false,
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": false,
            "bAutoWidth": false,
            "language": {
              "info":     false,
              "search": "Tìm kiếm:",
              'paginate': {
                'previous': '<<',
                'next': '>>'
              }
            },
          });
          $('#dataTables-example_filter input').addClass('form-control');
      });
  </script>
  <!-- End custom js for this page-->
</body>

</html>
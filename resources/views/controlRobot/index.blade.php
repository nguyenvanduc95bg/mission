<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MViBot</title>
    <style>
        html body{
            height: 1200px;
            width: 1830px;
        }
        body {
        margin: 0;
        font-family: 'Times New Roman', Times, serif;
        text-align: center;
        justify-content: center;
        display: flex;
        background-image: url('resources/image/1.jpg');
        background-repeat: no-repeat; 
        background-size: 1830px 1200px;   
        }
        .topnav {
        overflow: hidden;
        background-color: #333;
        }
        .topnav a {
        float: left;
        color: #f2f2f2;
        text-align: center;
        padding: 14px 16px;
        text-decoration: none;
        font-size: 20px;
        }
        .topnav a:hover {
        background-color: #ddd;
        color: black;
        }
        .topnav a.active {
        background-color: #27cab5;
        color: white;
        }
        .a-container{
            width: 40%;
            display: flex;
            flex-wrap: wrap;
            width: max-content;
            justify-content: center;
            margin-top: 50px;
            margin-bottom: 150px;
            text-decoration-line: none;
        }
        .a-container>a{
            font-family: 'Times New Roman', Times, serif;
            font-size: 40px;
            margin: 20px;
            background-color: aquamarine;
            border: none;
            height: 60px;
            border-radius: 10px;
            padding: 10px;
            text-decoration-line: none;
        }
        .a-container>a:hover{
            background-color: rgb(5, 165, 165);
            
        }
        .container{
            width: 100%;
            height: max-content;
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            }
        .ground{
           border-style: groove;
           height: max-content;
           width: 700px;
           margin-top: 400px;  
           color:coral;
           background-color: rgba(32, 222, 255, 0.8)
            
        }
        .image3{
        position: absolute;
        top: 8px;
        left: 1650px;
        }
        .image2{
            position: absolute;
            top: 8px;
            left: 80px;
            font-size: 18px;
        }
        .image1{
            position: absolute;
            top: 8px;
            left: 8px;
            font-size: 18px;
        }
        .name{
            position: absolute;
            top: 60px;
            left: 80px;
            width: 1530px;
            font-size: 30px;
            color: blue;

        }
        mark { 
        background-color: yellow;
        color: blue;
        }
    </style>
</head>
<body>
    <div class = "name">
    <p id="name" style="font-size: 100px;width: 1700px;"><strong><mark>MViBot - Smart AMR for logistic</mark> </strong>  </p>
    </div>
    <div class = "image1">
    <img src="./resources/image/Picture1.png" alt="Logo1" width="50px" height="50px" >
    </div>
    <div class = "image2">
    <img src="./resources/image/Picture2.png" alt="Logo2" width="50px" height="50px" >
    </div>
    <div class = "image3">
    <img src="./resources/image/Picture3.png" alt="Logo3" >
    </div>
    <div class="ground">
    <p id="show" style="font-size: 60px;width: 700px;  "><strong>Choose Mode</strong> </p>
    <div class="container">
        <div class="a-container">
            <a href="/control-robot/index2" onclick="Run('')" style="color: black;">NAVIGATION</a> 
            <a href="/control-robot/index1" onclick="Run('')" style="color: black;">MAPPING</a>
        </div>
    </div>
</body>
</html>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<script src="src/jquery.min.js"></script>
<script type="text/javascript" src="src/three.min.js"></script>
<script type="text/javascript" src="src/eventemitter2.min.js"></script>
<script type="text/javascript" src="src/roslib.min.js"></script>
<script type="text/javascript" src="src/ros3d.min.js"></script>
<script type="text/javascript" src="src/nipplejs.js"></script>
<script type="text/javascript" src="src/easeljs.min.js"></script>
<script type="text/javascript" src="src/ros2d.min.js"></script>

<script src="src/three.js"></script>
<script src="src/STLLoader.js"></script>
<script src="src/eventemitter2.js"></script>
<script src="src/ColladaLoader.js"></script>

<script src="src/roslib.js"></script>
<script src="src/ros3d.js"></script>

<script src="ip.js"></script>
<script src="ros.js"></script>
<script type="text/javascript" type="text/javascript">
   var pose,pose_pub,pose_msg
   var point,point_pub,point_msg
   var x=0,y=0,z=0,w=1
   function display_pose(x,y,z,w)
   {
      pose_msg.pose.position.x=x;
      pose_msg.pose.position.y=y;
      pose_msg.pose.position.z=0;
      pose_msg.pose.orientation.x=0;
      pose_msg.pose.orientation.y=0;
      pose_msg.pose.orientation.z=z;
      pose_msg.pose.orientation.w=w;
      pose_pub.publish(pose_msg);
   }
   function display_point(x,y)
   {
      point_msg.point.x=x;
      point_msg.point.y=y;
      point_pub.publish(point_msg);
   }
   function save_path(){
      alert("Save Path:");
   }
   function run_map(){
      alert("Run Map:"+document.getElementById("map_name").value);
   }
   function run_test(){
      alert("Run Path:");
   }
   window.onload = function () {
      connecting();     // connecting ros server
      init(ip);         // connect via ip 

      // display point
      point = new ROS3D.Point({
      ros : ros,
      rootObject : viewer.scene,
      tfClient : tfClient,
      topic: "/point_pub",
      color: 0xCD853F,
      queue_size: 1,
      throttle_rate: 1000,	
      radius: 0.25,
      });
      point_pub = new ROSLIB.Topic({
        ros : ros,
        name : "/point_pub",
        messageType : 'geometry_msgs/PointStamped',
        queue_size: 1,
      });
      point_msg = new ROSLIB.Message({
         header: {
            frame_id: "/map",
          },
         point: {
            x: 0,
            y: 0,
            z: 0,
          }
      });
      //

      // display pose
      pose = new ROS3D.Pose({
         ros:ros,
         rootObject : viewer.scene,
         tfClient : tfClient,
         topic: "/pose_pub",
         headDiameter: 0.5,
         shaftDiameter:0.1,
         length: 2,
      });
      pose_pub = new ROSLIB.Topic({
        ros : ros,
        name : "/pose_pub",
        messageType : 'geometry_msgs/PoseStamped',
        queue_size: 1,
      });
      pose_msg = new ROSLIB.Message({
         header: {
            frame_id: "/map",
          },
          pose: {
            position:{
               x:0,
               y:0,
               z:0,
            },
            orientation:{
               x:0,
               y:0,
               z:0,
               w:1,
            },
          }
      });
      //
      // disable element
      document.getElementById("direction").style.visibility = 'visible';
      var nodes = document.getElementById("core").getElementsByTagName('*');
      for(var i = 0; i < nodes.length; i++){
         nodes[i].disabled = false;
      }

      // check map exit
      var array = ['map1','map2','map3'];
      //Create and append the options
      for (var i = 0; i < array.length; i++) {
         var option = document.createElement("option");
         option.value = array[i];
         option.text = array[i];
         document.getElementById("map_name").appendChild(option);
      }

      
   }
</script>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="https://fonts.googleapis.com/css2?family=Readex+Pro:wght@300&family=Ubuntu&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" integrity="sha512-Fo3rlrZj/k7ujTnHg4CGR2D7kSs0v4LLanw2qksYuRlEzO+tcaEPQogQ0KaoGN26/zrn20ImR1DfuLWnOo7aBA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<title>MviBot</title>
<link rel="icon" href="./resources/image/logo.png">
<style>
   .in_ {
        position:relative;
        left: 0px;
        width: 100px;
        font-size: 30px;
    }
    .range_ {
        position:relative;
        left:  0px;
        width: 600px;
    }
    .row{
         position:relative;
         top: 40px;
    }
    .row1{
         position:relative;
         top: 10px;
         font-size: 20px;
    }
</style>
</head>
<body>
<div id="zone_joystick"></div>   

<div id="direction" style="position: absolute; top: 325px; left: 1420px; visibility: hidden; z-index: -2;">
   <div>
      <img src="./resources/image/Picture4.png" alt="Logo" width="50px" height="50px" style="position: absolute; top: -150px; left: 0px;">  
   </div>
   <div>
      <img src="./resources/image/Picture6.png" alt="Logo" width="50px" height="50px" style="position: absolute; top: 150px; left: 0px;">  
   </div>
   <div>
      <img src="./resources/image/Picture5.png" alt="Logo" width="50px" height="50px" style="position: absolute; top: 0px; left: -150px;">  
   </div>
   <div>
      <img src="./resources/image/Picture7.png" alt="Logo" width="50px" height="50px" style="position: absolute; top: 0px; left: 150px;">  
   </div>  
</div>

<div id="core" style="position: relative; left:0px;" >
 
   <div id="map"></div>
   
   <div style="position: relative; top: 0px; left: 1100px;">
      <div class="row">
         <div style="position: relative; top:-100px; left: 80px; ">
            <input type="button" value="Save Path" style="font-size: 30px; width: 200px; 
            height: 50px; color:#104e8b; background-color: #ffc125; border: 2px solid #ff7f24; border-radius: 40px;left: 50px; position:relative;top:20px;" onclick="save_path()">
            <input type="button" value="Run Path" style="font-size: 30px; width: 200px; height: 50px; position: relative; 
            left: 100px; color: #104e8b; background-color: #ffc125; border: 2px solid #ff7f24; border-radius: 40px;top:20px;" onclick="run_test()">
         </div>
      </div>
      <div style="position: relative; top: -475px;">
         <select id="map_name" style="font-size: 30px; width: 175px; height: 50px; color: #104e8b;">
         </select>
         <input type="button" value="Start" style="font-size: 30px; width: 100px; height: 50px; position: relative; 
         left: 10px;color:#fff; background-color: #008b00; border: 2px solid #ee7621; border-radius: 20px;" 
         onclick="run_map()">
         <b style="font-size: 30px; width: 200px; height: 50px; position: relative; left: 20px;color:#104e8b;">Name Path</b>
         <input type="text" id="map_point" style="font-size: 30px; width: 200px; height: 50px; position: relative; left: 30px;">
         </input>
      </div>
      <div style="position: relative; top: -475px;">
            <p style="width: 715px; height: 270px; background-color: #c0c0c0;"></p>
      </div>
   </div>
</div>
</body>
</html>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<script src="src/jquery.min.js"></script>
<script type="text/javascript" src="src/three.min.js"></script>
<script type="text/javascript" src="src/eventemitter2.min.js"></script>
<script type="text/javascript" src="src/roslib.min.js"></script>
<script type="text/javascript" src="src/ros3d.min.js"></script>
<script type="text/javascript" src="src/nipplejs.js"></script>
<script type="text/javascript" src="src/easeljs.min.js"></script>
<script type="text/javascript" src="src/ros2d.min.js"></script>

<script src="src/three.js"></script>
<script src="src/STLLoader.js"></script>
<script src="src/eventemitter2.js"></script>
<script src="src/ColladaLoader.js"></script>

<script src="src/roslib.js"></script>
<script src="src/ros3d.js"></script>
<script src="ip.js"></script>
<script type="text/javascript" type="text/javascript">
    var ros,viewer,tfClient,gridClient,laser,urdfClient
    var cmd_vel_listener,move,listener,listener2,listener3
    var talker
    function init(id_ad) {
    viewer = new ROS3D.Viewer({
      divID : 'map',
      width : 1000,
      height : 600,
      background: '#4F4F4F',
      antialias : true,
    });
    viewer.addObject(new ROS3D.Grid());
    gridClient = new ROS3D.OccupancyGridClient({
        ros : ros,
        rootObject : viewer.scene,
        continuous: false,
        tfClient: tfClient,
        queue_size: 1,
	throttle_rate: 3000,
      });
    tfClient = new ROSLIB.TFClient({
        ros : ros,
        rate : 10,
        fixedFrame : '/map',
        queue_size: 1,
        });
    laser = new ROS3D.LaserScan({
        ros : ros,
        topic: "/scan",
        rootObject : viewer.scene,
        tfClient: tfClient,   
        max_pts: 2000,
        material : { size: 0.5, color: 0xff0000 },
        queue_size: 1,
	throttle_rate: 1000,
      });
    urdfClient = new ROS3D.UrdfClient({
        ros : ros,
        tfClient : tfClient,
        path : id_ad,
        rootObject : viewer.scene,
        loader : ROS3D.COLLADA_LOADER_2,
        queue_size: 1,
	throttle_rate: 1000,
      });
    cmd_vel_listener = new ROSLIB.Topic({
        ros : ros,
        name : "/cmd_vel",
        messageType : 'geometry_msgs/Twist',
        queue_size: 1,
      });
    listener = new ROSLIB.Topic({
        ros : ros,
        name : '/mode',
        messageType : 'std_msgs/String',
        queue_size: 1,
	throttle_rate: 1000,
        });
    listener.subscribe(function(message) {
        var txt=message.data;
        if(message.data[0]=='2') window.location.href="/control-robot/index2";
        else{
          if(message.data[0]=='0'){
            document.getElementById("run_stop").innerText="Run";
            document.getElementById("run_stop").classList.remove('button2');
            document.getElementById("run_stop").classList.add('button1');
            document.getElementById("name_map").value="";
            document.getElementById("name_map").disabled=true;
          }
          if(message.data[0]=='1'){
            document.getElementById("run_stop").innerText="Stop";
            document.getElementById("run_stop").classList.remove('button1');
            document.getElementById("run_stop").classList.add('button2');
            document.getElementById("name_map").disabled=false;
          }
        }
        });
    listener2 = new ROSLIB.Topic({
        ros : ros,
        name : '/set_led',
        messageType : 'std_msgs/Float32MultiArray',
        throttle_rate: 1000,
        queue_size: 1,
        });
    listener2.subscribe(function(message) {
        if(Number(message.data[0])==1) {
          document.getElementById("green").style.backgroundColor="#00FF00";
          document.getElementById("green").style.boxShadow="5px 5px white";	
        }
        else {
          document.getElementById("green").style.backgroundColor="#005500";
          document.getElementById("yellow").style.boxShadow="5px 5px green";
        }
        if(Number(message.data[1])==1) {
          document.getElementById("red").style.backgroundColor="#FF0000";
          document.getElementById("red").style.boxShadow="5px 5px white";
        }
        else {
          document.getElementById("red").style.backgroundColor="#660000";
          document.getElementById("red").style.boxShadow="5px 5px gray";
        }
        if(Number(message.data[2])==1) {
          document.getElementById("yellow").style.backgroundColor="#FFFF00";
          document.getElementById("yellow").style.boxShadow="5px 5px white";
        }
        else {
          document.getElementById("yellow").style.backgroundColor="#8B8B00";
          document.getElementById("yellow").style.boxShadow="5px 5px gray";
        }
      });
    listener3 = new ROSLIB.Topic({
        ros : ros,
        name : '/sen_sensor',
        messageType : 'std_msgs/Float32MultiArray',
        throttle_rate: 1000,
        queue_size: 1,
        });
    listener3.subscribe(function(message) {
        battery((Number(message.data[3])*6-25.0)/2.0*100);
	if(Number(message.data[0])==1) i=1;
      });
    talker = new ROSLIB.Topic({
        ros : ros,
        name : '/command',
        messageType : 'std_msgs/String'
        });
    }
    //
    move = function (linear, angular) {
      var twist = new ROSLIB.Message({
          linear: {
            x: linear,
            y: 0,
            z: 0
          },
          angular: {
            x: 0,
            y: 0,
            z: angular
          }
        });
        cmd_vel_listener.publish(twist);
    }
    //
    var createJoystick = function () {
      var options = {
        zone: document.getElementById('zone_joystick'),
        threshold: 0.1,
        position: { left: 1450 + 'px',
        	     top: 950 + 'px' },
        mode: 'static',
        size: 400,
        color: 'black',
      };
      manager = nipplejs.create(options);
      linear_speed = 0;
      angular_speed = 0;
      self.manager.on('start', function (event, nipple) {
        console.log("Movement start");
          timer = setInterval(function () {
    		move(linear_speed, angular_speed);
  	  }, 500);
      });
      self.manager.on('move', function (event, nipple) {
          console.log("Moving");
          max_linear = 0.4; // m/s
	    max_angular = 0.314; // rad/s
	    max_distance = 200; // pixels;
	    linear_speed = Math.sin(nipple.angle.radian) * max_linear * nipple.distance/max_distance;
	    angular_speed = -Math.cos(nipple.angle.radian) * max_angular * nipple.distance/max_distance;
      });
      self.manager.on('end', function () {
        console.log("Movement end");
		  if (timer) {
	    clearInterval(timer);
	    } 
	    self.move(0, 0);
	    });
      }
    //
    var once=true;
    function connecting(ip) {
        if (once) {
          ros = new ROSLIB.Ros({
          url: 'ws://'+ip+':9090'
          });
          ros.on('connection', function() {
                init('ws://'+ip+':9090'); 
                createJoystick();
                alert("Connected");
                document.getElementById("red").style.backgroundColor="Maroon";
                document.getElementById("green").style.backgroundColor="Green";
                document.getElementById("yellow").style.backgroundColor="Olive";
                //
                document.getElementById('abc').style.visibility = 'visible';
                once=false; 
          });
          ros.on('error', function() {
                alert("Error");
                once=true; 
                window.location.href="/control-robot/index";                
          }); 
          ros.on('close', function() {
                alert("Disconnected");
                once=true; 
                window.location.href="/control-robot/index";
          });   
        } 
      }
    function run_stop(){
      var command = new ROSLIB.Message({
      data: ""
      });
      if(document.getElementById("run_stop").innerText=='Run'){
        command.data='1';
        talker.publish(command);
        alert("Run mode"); 																																																																																																																																																																																																																																																																																																																																																																																																																																																																						
      } 
      if(document.getElementById("run_stop").innerText=='Stop'){
        command.data='0';
        talker.publish(command);
        alert("Stop mode");
      }
    }
    function mled(s_green,s_yellow,s_red){

    }
    function save_map() {
      if(document.getElementById("name_map").value!="")
      {
      var txt = new ROSLIB.Message({
          data: '3'+document.getElementById("name_map").value,
        });
      pub = new ROSLIB.Topic({
        ros : ros,
        name : '/command',
        messageType : 'std_msgs/String'
        });
      pub.publish(txt);
      alert("Save map:"+document.getElementById("name_map").value);
     }
    }
    function reload() {
		  gridClient.subscribe();	
	  }
    gridClient.on('change', function(){
      gridClient.unsubscribe();
    });
</script>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MviBot</title>
<style>
    html body {
        height: 1200px;
        width: 1830px;
    }
    body {
        margin: 0;
        font-family: 'Times New Roman', Times, serif;
        text-align: center;
        background-image: url('resources/image/3.jpg');
        background-repeat: no-repeat;
        background-size: 1830px 1500px;
    }
    .topnav {
        overflow: hidden;
        background-color: #333;
    }
    .topnav a {
        float: left;
        color: #f2f2f2;
        text-align: center;
        padding: 14px 16px;
        text-decoration: none;
        font-size: 20px;
    }
    .topnav a:hover {
        background-color: #ddd;
        color: black;
    }

    .topnav a.active {
        background-color: #27cab5;
        color: white;
    }
    .button-container {
        width: 100%;
        display: flex;
        justify-content: center;
        margin-top: 50px;
    }
    .button-container>button {
        font-family: 'Times New Roman', Times, serif;
        width: 20%;
        font-size: 25px;
        margin: 30px;
        background: #3366FF;
        border: none;
        border-radius: 5px;
        height: 120px;
        color: #fff;
        justify-content: center;
        margin-top: 100px;          
    }
    .button-container>button:hover {
        opacity: 0.75;
    }
    .container {
        width: 100%;
        height: max-content;
        flex-wrap: wrap;
        position: relative;
        bottom: auto;
    }
    .camera {
        margin-top: 20px;
        width: 100%;
        min-width: 500px;
        display: flex;
        justify-content: center;
    }
    @media (max-width: 800px) {
        .button-container {
            width: 100%;
        }

        .camera {
            width: 100%;
        }
    }
    .input-containner {
        width: max-content;
        min-height: 115px;
        padding: 20px;
        position: absolute;
        top: 2px;
        z-index: 1;
        margin: 0 auto;
        background: #CD853F;
        left: 0;
        right: 0;
        text-align: center;
        justify-content: center;
        display: none;
    }
    .title {
        margin-top: 0;
    }

    #input {
        padding: 0 20px;
    }

    #myTexts {
        width: 500px;
        height: 200px;
    }
    .image3{
        position: absolute;
        top: 8px;
        left: 1650px;
    
    }
    .image2{
        position: absolute;
        top: 8px;
        left: 80px;
        font-size: 18px;
    }
    .image1{
        position: absolute;
        top: 8px;
        left: 8px;
        font-size: 18px;
    }
    .name{
            position: absolute;
            top: 60px;
            left: 80px;
            width: 1530px;
            font-size: 30px;
            color: blue;

        }
    .row1{
            position: absolute;
            top: 350px;
            height: 60px;
            width: 200px;
            font-size: 40px;
            border-width: 5px;
            border-radius: 15px;
    }
    .button1 {
            display: inline-block;
            padding: 15px 25px;
            font-size: 40px;
            cursor: pointer;
            text-align: center;
            text-decoration: none;
            outline: none;
            color: #fff;
            background-color: #4CAF50;
            border: 10px;
            border-radius: 15px;
            box-shadow: 0 9px #999;
    }
    .button1:hover {background-color: #3e8e41}
  
    .button1:active {
      background-color: #3e8e41;
      box-shadow: 0 5px #666;
      transform: translateY(4px);
    }
    .button2 {
    display: inline-block;
    padding: 15px 25px;
    font-size: 40px;
    cursor: pointer;
    text-align: center;
    text-decoration: none;
    outline: none;
    color: #fff;
    background-color: #af534c;
    border: none;
    border-radius: 15px;
    box-shadow: 0 9px #999;
    }
    .button2:hover {background-color: #8e3e3e}
    .button2:active {
    background-color: #8e3e3e;
    box-shadow: 0 5px #666;
    transform: translateY(4px);
    }
    .button3 {
    display: inline-block;
    padding: 15px 25px;
    font-size: 40px;
    cursor: pointer;
    text-align: center;
    text-decoration: none;
    outline: none;
    color: #fff;
    background-color: #0051ff;
    border: none;
    border-radius: 15px;
    box-shadow: 0 9px #999;
    }
    .button3:hover {background-color: #002d70}
    .button3:active {
    background-color: #002d70;
    box-shadow: 0 5px #666;
    transform: translateY(4px);
    }
    .button4 {
    display: inline-block;
    padding: 15px 25px;
    font-size: 40px;
    cursor: pointer;
    text-align: center;
    text-decoration: none;
    outline: none;
    color: #fff;
    background-color: #ffc400;
    border: none;
    border-radius: 15px;
    box-shadow: 0 9px #999;
    }
    .button4:hover {background-color: #7c5f00}
    .button4:active {
    background-color: #7c5f00;
    box-shadow: 0 5px #666;
    transform: translateY(4px);
    }
    .led {
    background-color: rgb(122, 118, 118);
    border: solid thin rgb(0, 0, 0);
    border-width: 5px;
    border-radius: 15px;
    box-shadow: 5px 5px rgb(102, 102, 102);
    }
    .battery {
      padding: 10px 10px;
      width: 340px;
      border: solid thin rgb(0, 0, 0);
      border-width: 5px;
      position: absolute;
      background-color:rgb(182, 170, 170);
    }
    .battery:after {
      content: " ";
      top: 25px;
      right: -15px;
      height: 30px;
      width: 10px;
      position: absolute;
      background: rgb(0, 0, 0);
    }

    .bar {
      cursor: pointer;
      display: inline-block;
      width: 0;
      border: solid thin rgb(0, 0, 0);
      padding: 11px;
      height: 30px;
      background: transparent;
      transition: background 1s;
      border-width: 4px;
    }

    .bar.active {
      background: limegreen;
    }
    .bar.active1 {
      background: yellow;
    }
    .bar.active2 {
      background: red;
    }
</style>
</head>
<body>
  <div class = "name">
    <p id="name" style="font-size: 100px;width: 1700px;"><strong><mark>MViBot - MAPPING</mark> </strong>  </p>
  </div>
  <div class="image1">
    <img src="./resources/image/Picture1.png" alt="Logo" width="50px" height="50px">
  </div>

  <div class="image2">
    <img src="./resources/image/Picture2.png" alt="Logo1" width="50px" height="50px">
  </div>

  <div class="image3">
    <img src="./resources/image/Picture3.png" alt="Logo2" >
  </div>    

  <div id="abc" style="visibility: hidden;">
    <img src="./resources/image/Picture4.png" alt="Logo" width="50px" height="50px" style="position: absolute; top: 780px; left: 1425px;">  
    <img src="./resources/image/Picture6.png" alt="Logo" width="50px" height="50px" style="position: absolute; top: 1080px; left: 1425px;">  

    <img src="./resources/image/Picture5.png" alt="Logo" width="50px" height="50px" style="position: absolute; top: 930px; left: 1275px;">  
    <img src="./resources/image/Picture7.png" alt="Logo" width="50px" height="50px" style="position: absolute; top: 930px; left: 1580px;">  
  </div>

  <div id="map" style="position: absolute; left: 100px; opacity: 1.0; top: 650px;"></div>
  <div id="zone_joystick"style=""></div>
  <button class="row1 button1" style="vertical-align:middle; left: 200px; top: 460px; width: 300px; height: 90px;" id="run_stop" onclick="run_stop()"><span>Run</span></button>
  <p class="row1 led " style="left: 900px; top: 420px; text-align: center;" id="red"></p>
  <p class="row1 led " style="left: 1150px; top: 420px;text-align: center;" id="green"></p>
  <p class="row1 led " style="left: 1400px; top: 420px;text-align: center;" id="yellow"></p>
  <div id="project_container">
    <div id="projectbox" style="position: absolute; top: 330px; left:1225px;">
      <div class='battery'>
        <div class='bar' data-power='10'></div>
        <div class='bar' data-power='20'></div>
        <div class='bar' data-power='30'></div>
        <div class='bar' data-power='40'></div>
        <div class='bar' data-power='50'></div>
        <div class='bar' data-power='60'></div> 
        <div class='bar' data-power='70'></div>
        <div class='bar' data-power='80'></div>
        <div class='bar' data-power='90'></div>
        <div class='bar' data-power='100'></div>
      </div>
    </div>
  </div>
  <mark><p class="row1" style="left: 900px; top: 270px; font-size: 70px; text-align: center; ">BATTERY</p></mark>
  
  <p class="row1" style="width: 250px; top: 330px; left:530px; font-size: 40px;"><strong>Name Map</strong></p>
 
  <p style="width: 200px; height: 180px; background-color:burlywood; border-radius: 20px; border-width: 5px; border-style: solid; position:absolute; top:405px; left:545px;"></p>
  <button class="row1 button4" style="vertical-align:middle; left: 560px; top: 530px; font-size:40px; width: 175px; color: black;" onclick="save_map()"><span><strong>SAVE</strong></span></button>
  <button class="row1 button4" style="vertical-align:middle; left: 925px; top: 650px; font-size:40px; width: 175px; color: black;" onclick="reload()"><span><strong>Reload</strong></span></button>
  <input type="text" id="name_map" class="row1" style="left: 565px; top: 440px; width: 150px; height: 60px;"><br><br>
</body>
</html>

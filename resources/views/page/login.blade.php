<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>HITECH </title>
  <base href="{{asset('')}}">
  <!-- plugins:css -->
  <link rel="stylesheet" href="/template/admin/vendors/feather/feather.css">
  <link rel="stylesheet" href="/template/admin/vendors/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="/template/admin/vendors/ti-icons/css/themify-icons.css">
  <link rel="stylesheet" href="/template/admin/vendors/typicons/typicons.css">
  <link rel="stylesheet" href="/template/admin/vendors/simple-line-icons/css/simple-line-icons.css">
  <link rel="stylesheet" href="/template/admin/vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="/template/admin/css/vertical-layout-light/style.css">
  <!-- endinject -->
  <link rel="icon" type="image/x-icon" href="/img/favicon.png">
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth px-0">
        <div class="row w-100 mx-0">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-light text-left py-5 px-4 px-sm-5">
              <div class="brand-logo">
                <img src="/img/logo.png" alt="">
              </div>
              <h4 class="text-center">Chào mừng bạn đến với HITECH!</h4>
              @if(count($errors)>0)
              <div class="alert alert-danger">
                @foreach($errors->all() as $err)
                {{$err}}
                @endforeach
              </div>
              @endif
              @if(Session::has('error'))
              <div class="alert alert-danger">{{Session::get('error')}}</div>
              @endif
              <form class="pt-3"  action="/login" method="post">
              	{!!csrf_field()!!}
                <div class="form-group">
                  <input type="email" class="form-control form-control-lg" name="email" placeholder="Email">
                </div>
                <div class="form-group">
                  <input type="password" class="form-control form-control-lg" name="password" placeholder="Mật Khẩu">
                </div>
                <div class="mt-3">
                  <input type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" value="Đăng nhập">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <style> 
  .content-wrapper {
    background: url('/img/background.jpg');
    background-repeat: no-repeat;
    background-size: 100%;
  }
  </style>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="/template/admin/vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page -->
  <script src="/template/admin/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="/template/admin/js/off-canvas.js"></script>
  <script src="/template/admin/js/hoverable-collapse.js"></script>
  <script src="/template/admin/js/template.js"></script>
  <script src="/template/admin/js/settings.js"></script>
  <script src="/template/admin/js/todolist.js"></script>
  <!-- endinject -->
</body>

</html>

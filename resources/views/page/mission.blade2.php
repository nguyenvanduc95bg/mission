@extends('layouts.master')
@section('content')
<div class="row">
    <div class="content-header">
        <div class="content-header__logo">
            <h2>Missions</h2>
            <span>Create and edit missions</span>
            @if(Session::has('success'))
            <div class="alert alert-success">{{Session::get('success')}}</div>
            @endif
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#createMission"><i class="fas fa-plus"></i>Create mission</button>
        </div>
    </div>
    <div class="content-main mt-2">
        <div class="row">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header">
                        <label for="">Show missions</label><br>
                        <div class="row">
                            <div class="col-md-8">
                                <select class="form-control" name="" id="">
                                    <option value="">All missions</option>
                                    <option value="">Change robot</option>
                                    <option value="">Test mission</option>
                                    <option value="">Transport Goods</option>
                                </select>
                            </div>
                            <div class="col-md-4 group-button">
                                <button class="btn btn-primary" data-toggle="modal" data-target="#missionGroup" type="button">Create / Edit groups</button>
                                <a class="btn btn-danger" href="/add-point">Add point</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive mt-2">
                            <table class="table table-striped" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($missions as $item)
                                    <tr>
                                        <td class="py-1">
                                            <h5>{{$item->name}}</h5>
                                            <span>{{$item->type}}</span>
                                        </td>
                                        <td class="center">
                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                <a href="#" class="btn btn-secondary showdetails" data-toggle="modal" data-target="#updateMission" data-id="{{$item->id}}" data-name="{{$item->name}}" data-description="{{$item->description}}" data-type="{{$item->type}}" data-group="{{$item->mission_group_id}}">
                                                    <i class="mdi mdi-grease-pencil"></i>
                                                </a>
                                                <a href="/delete-mission/{{$item->id}}" onclick="return confirm('Are you sure?')" class="btn btn-danger">
                                                    <i class="mdi mdi-delete"></i>
                                                </a>
                                                <a data-toggle="modal" data-target="#runMission" data-id="{{$item->id}}" href="/runMission/{{$item->id}}" class="btn btn-success run-mission">
                                                    <i class="mdi mdi-note"></i>
                                                </a>
                                                {{-- <a href="add-point/{{$item->id}}" class="btn btn-primary ">
                                                    <i class="mdi mdi-plus-box"></i>
                                                </a> --}}
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>       
            </div>
            <div class="col-md-5">
            <div class="card">
                    <div class="card-header">
                        <label for="">List Point</label><br>
                    <div class="card-body">
                    <!-- <table class="table table-striped table-reponsive" id="dataTables-example">  -->   
                        <div class="table-responsive mt-2">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Point</th>
                                </tr>
                                </thead>
                                
                                <tbody>
                                @foreach($savepoints as $item)
                                    <tr>
                                        <td class="py-1">
                                            {{$item->name}}
                                        </td>
                                        <td class="center">
                                            
                                            {{$item->toado}}
                                        </td>
                                        <td class="center">
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            <a href="/delete-savepoint/{{$item->id}}" onclick="return confirm('Are you sure?')" class="btn btn-danger">
                                                <i class="mdi mdi-delete"></i>
                                            </a>
                                            <a href="/runSavePoint/{{$item->id}}" class="btn btn-success">
                                                <i class="mdi mdi-note"></i>
                                            </a>
                                        </div>
                                    </td>
                                @endforeach
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="createMission" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Create Mission</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
            <form action="/create-mission" method="post">
                @csrf
            <div class="modal-body create-body">
                <label for="">Name</label><br>
                <input type="text" name="mission_name" class="form-control">
                <label for="">Description</label><br>
                <input type="text" name="mission_description" class="form-control">
                <div class="row">
                    <div class="mission-create col-md-6 mb-2">
                        <label for="">Group</label><br>
                        <select name="group" id="" class="form-control">
                            @foreach($mission_groups as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                        <button data-toggle="modal" data-target="#missionGroup" class="form-btn btn btn-secondary" type="button">Create/Edit</button>
                    </div>
                    <div class="mission-create-custom col-md-6">
                        <label for="">Type</label><br>
                        <select name="type" id="" class="form-control">
                            <option value="MViBot HQ">MViBot HQ</option>
                            <option value="Move">Move</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success">Create mission</button>
            </div>
            

            </form>
            </div>
        </div>
    </div>
    
    
    <div class="modal fade" id="updateMission" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Update Mission</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
            <form action="/update-mission" method="post">
                @csrf
            <div class="modal-body create-body">
                <input type="hidden" name="mission_id" id="mission_id">
                <label for="">Name</label><br>
                <input type="text" name="mission_name" class="form-control" id="mission_name">
                <label for="">Description</label><br>
                <input type="text" name="mission_description" class="form-control" id="mission_description">
                <div class="row">
                    <div class="mission-create col-md-6">
                        <label for="">Group</label><br>
                        <select name="group" id="group" class="form-control">
                            @foreach($mission_groups as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mission-create-custom col-md-6">
                        <label for="">Type</label><br>
                        <select name="type" id="type" class="form-control">
                            <option value="MViBot HQ">MViBot HQ</option>
                            <option value="Move">Move</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success">Create mission</button>
            </div>
            </form>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="missionGroup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <div>
                    <h5 class="modal-title" id="exampleModalLongTitle">Mission groups</h5>
                    <div><span style="font-size: 12px; width: 250px; display: inline-block;">You can edit group names, delete groups or create new groups</span></div>
                </div>
                <button data-toggle="modal" data-target="#createMissionGroup" class="btn btn-primary">Create groups</button>
            </div>
            <div class="modal-body create-body mission-group">
                @foreach($mission_groups as $mg)
                <div class="mission-group-item">
                    <h6>{{$mg->name}}</h6>
                    <div class="mission-group-control">
                        {{-- <button class="btn btn-secondary" type="button">
                            <i class="fas fa-pencil-alt"></i>
                        </button> --}}
                        <a href="/delete-misson-group/{{$mg->id}}" onclick="return confirm('Are you sure?')" class="btn btn-secondary" type="button">
                            <i class="mdi mdi-delete"></i>
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-success">OK</button>
            </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="createMissionGroup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div>
                        <h5 class="modal-title" id="exampleModalLongTitle"><i class="fas fa-cogs mr-2"></i>Create mission groups</h5>
                        <div><span style="font-size: 12px">If you don't want to use any of the default group names, you can create your own group(s) and save missions
                            here. New groups will be shown in the top bar next to the default groups and contain any mission(s) you want to
                            add to it</span></div>
                    </div>
                </div>
                <form action="/create-mission-groups" method="POST"> 
                    @csrf
                    <div class="modal-body create-body create-mission-group">
                        <label for="">Name</label>
                        <input name="group_mission_name" class="form-control" type="text">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Create group mission</button>
                    </div>
                </form>
            
            </div>
        </div>
    </div>

     
    <div class="modal fade" id="runMission" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Run mission</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="run-mission" method="post">
                    @csrf
                <div class="modal-body create-body">
                    <input type="hidden" name="run_mission_id" id="run_mission_id">
                    <label for="">Times</label><br>
                    <input type="number" name="unit" class="form-control" id="unit">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Send</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
    <script type="text/javascript">
        var elements = document.getElementsByClassName("showdetails");
        var myFunction = function() {
            var id = this.getAttribute("data-id");
            var name = this.getAttribute("data-name");
            var description = this.getAttribute("data-description");
            var type = this.getAttribute("data-type");
            var group = this.getAttribute("data-group");
            document.getElementById('mission_id').value=id;
            document.getElementById('mission_name').value=name;
            document.getElementById('mission_description').value=description;
            document.getElementById('type').value=type;
            document.getElementById('group').value=group;
        };

        for (var i = 0; i < elements.length; i++) {
            elements[i].addEventListener('click', myFunction, false);
        }

        var runMission = document.getElementsByClassName("run-mission");
        var runMissionFunction = function() {
            var id = this.getAttribute("data-id");
            document.getElementById('run_mission_id').value=id;
        };

        for (var i = 0; i < runMission.length; i++) {
            runMission[i].addEventListener('click', runMissionFunction, false);
        }

    </script>
@endsection
@extends('layouts.master')
@section('content')
<div class="rows">
    <div class="content-header">
        <div class="content-header__logo">
            <h2>Maps</h2>
            <span>Create and edit maps</span>
            @if(Session::has('success'))
            <div class="alert alert-success">{{Session::get('success')}}</div>
            @endif
            @if(Session::has('error'))
            <div class="alert alert-danger">{{Session::get('error')}}</div>
            @endif
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createMap"><i class="fas fa-plus"></i>Create map</button>
        </div>
    </div>
    <div class="content-main mt-2">
        <div class="row">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header">
                        <label for="">Show maps</label><br>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive mt-2">
                            <table class="table table-striped table-reponsive" id="dataTables-example">
                                <thead>
                                    <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">PointA</th>
                                    <th scope="col">PointB</th>
                                    <th scope="col">PointC</th>
                                    <th scope="col">PointD</th>
                                    <td></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1 ?>
                                    @foreach($maps as $item)
                                    <tr>
                                        <th scope="row">{{$i++}}</th>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->pointA}}</td>
                                        <td>{{$item->pointB}}</td>
                                        <td>{{$item->pointC}}</td>
                                        <td>{{$item->pointD}}</td>
                                        <td>
                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                <a href="#" class="btn btn-secondary showdetails" data-toggle="modal" data-target="#updateMap" data-id="{{$item->id}}" data-mapname="{{$item->name}}" data-pointa="{{$item->pointA}}" data-pointb="{{$item->pointB}}" data-pointc="{{$item->pointC}}" data-pointd="{{$item->pointD}}">
                                                    <i class="mdi mdi-grease-pencil"></i>
                                                </a>
                                                <a href="/delete-maps/{{$item->id}}" onclick="return confirm('Are you sure?')" class="btn btn-danger mx-2">
                                                    <i class="mdi mdi-delete"></i>
                                                </a>
                                                <a  href="/showMap/{{$item->id}}" class="btn btn-success ">
                                                    <i class="mdi mdi-note"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <form action="/import-map" method="POST" enctype='multipart/form-data'>
                    @csrf
                    <input type="file" required name="import_file" class="form-control" accept=".yaml">
                    <label for="" class="text-danger">* Choose file csv to import data</label><br>
                    <input type="submit" class="btn btn-primary mt-2" value="Import Maps">
                    <a href="/export-maps" class="btn btn-danger ml-2 mt-2">Export Maps</a>
                </form>
            </div>
        </div>
    </div>
</div>
    <div class="modal fade" id="createMap" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Create Map</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
            <form action="/create-map" method="post">
                @csrf
            <div class="modal-body create-body">
                <label for="">Name</label><br>
                <input type="text" name="map_name" class="form-control" required>
                <label for="">PointA</label><br>
                <input type="number" name="pointa" class="form-control" required>
                <label for="">PointB</label><br>
                <input type="number" name="pointb" class="form-control" required>
                <label for="">PointC</label><br>
                <input type="number" name="pointc" class="form-control" required>
                <label for="">PointD</label><br>
                <input type="number" name="pointd" class="form-control" required>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success">Create maps</button>
            </div>
            </form>
            </div>
        </div>
    </div>


    <div class="modal fade" id="updateMap" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Update Map</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
            <form action="/update-map" method="post">
                @csrf
                <div class="modal-body create-body">
                    <input type="hidden" name="map_id" id="map_id">
                    <label for="">Name</label><br>
                    <input type="text" name="map_name" class="form-control" id="map_name" required>
                    <label for="">PointA</label><br>
                    <input type="number" name="pointa" class="form-control" id="pointa" required>
                    <label for="">PointB</label><br>
                    <input type="number" name="pointb" class="form-control" id="pointb" required>
                    <label for="">PointC</label><br>
                    <input type="number" name="pointc" class="form-control" id="pointc" required>
                    <label for="">PointD</label><br>
                    <input type="number" name="pointd" class="form-control" id="pointd" required>
                </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success">Update map</button>
            </div>
            </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var elements = document.getElementsByClassName("showdetails");

        var myFunction = function() {
            var id = this.getAttribute("data-id");
            var name = this.getAttribute("data-mapname");
            var pointa = this.getAttribute("data-pointa");
            var pointb = this.getAttribute("data-pointb");
            var pointc = this.getAttribute("data-pointc");
            var pointd = this.getAttribute("data-pointd");
          
            document.getElementById('map_id').value=id;
            document.getElementById('map_name').value=name;
            document.getElementById('pointa').value=pointa;
            document.getElementById('pointb').value=pointb;
            document.getElementById('pointc').value=pointc;
            document.getElementById('pointd').value=pointd;
        };

        for (var i = 0; i < elements.length; i++) {
            elements[i].addEventListener('click', myFunction, false);
        }

    </script>
@endsection

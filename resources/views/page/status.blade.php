@extends('layouts.master')
@section('content')
    <meta charset="UTF-8">
    <!-- //<script src="control-robot/src/jquery.min.js"></script>  -->
    <script type="text/javascript" src="control-robot/src/three.min.js"></script>
    <script type="text/javascript" src="control-robot/src/eventemitter2.min.js"></script>
    <script type="text/javascript" src="control-robot/src/roslib.min.js"></script>
    <script type="text/javascript" src="control-robot/src/ros3d.min.js"></script>
    <script type="text/javascript" src="control-robot/src/nipplejs.js"></script>
    <script type="text/javascript" src="control-robot/src/easeljs.min.js"></script>
    <script type="text/javascript" src="control-robot/src/ros2d.min.js"></script>

    <script src="control-robot/src/three.js"></script>
    <script src="control-robot/src/STLLoader.js"></script>
    <script src="control-robot/src/eventemitter2.js"></script>
    <script src="control-robot/src/ColladaLoader.js"></script>

    <script src="control-robot/src/roslib.js"></script>
    <script src="control-robot/src/ros3d.js"></script>
    <script src="control-robot/ip.js"></script> 


    <script type="text/javascript" type="text/javascript">
    var listener
    ros = new ROSLIB.Ros({
        url: 'ws://'+'10.0.3.23'+':9090'
        });
 
    ros.on('connection', function() {
        alert("Connected");
    });
    ros.on('close', function() {
        alert("Disconnected");
        location.reload();
    });
    //setInterval(myTimer, 1000);

    //function myTimer() {    
      listener = new ROSLIB.Topic({
       ros : ros,
       name : '/IAM',
       messageType :'std_msgs/String'
        });
        listener.subscribe(function(message) {
            var txt1 
            txt1 = message.data;
            console.log(txt1);
            //txt = ["MB21_916b","0","1","1","1","-1","1","1"]
            const txt=txt1.split("|");
            console.log(txt);
            document.getElementById("serial").innerHTML = txt[0];
            document.getElementById("mode").innerHTML = txt[1];
            document.getElementById("volt").innerHTML = txt[2];
            document.getElementById("competi").innerHTML = txt[3];
            if (parseFloat(txt[4]) < 0) {
                document.getElementById("rada1").innerHTML = "<span class='badge badge-danger'> No Signal</span>";
            } else {
                document.getElementById("rada1").innerHTML = "<span class='badge badge-success'>Signal</span>";
            }
            if (parseFloat(txt[5]) < 0) {
                document.getElementById("rada2").innerHTML = "<span class='badge badge-danger'> No Signal</span>";
            } else {
                document.getElementById("rada2").innerHTML = "<span class='badge badge-success'>Signal</span>";
            }
            if (parseFloat(txt[6]) < 0) {
                document.getElementById("cam1").innerHTML = "<span class='badge badge-danger'> No Signal</span>";
            } else {
                document.getElementById("cam1").innerHTML = "<span class='badge badge-success'>Signal</span>";
            }
            if (parseFloat(txt[7]) < 0) {
                document.getElementById("cam2").innerHTML = "<span class='badge badge-danger'> No Signal</span>";
            } else {
                document.getElementById("cam2").innerHTML = "<span class='badge badge-success'>Signal</span>";
            }
            console.log(txt);
            //alert("a");
            // const myarray=txt.split("|");
            // console.log(myarray);
        });
        //sssssdelete listener;
        //delete listener.subscribe;
        //console.log("a");
    //}

</script>
<div class="rows">
    <div class="content-header">
        <div class="content-header__logo">
            <h2>Status</h2>
            @if(Session::has('success'))
            <div class="alert alert-success">{{Session::get('success')}}</div>
            @endif
            @if(Session::has('error'))
            <div class="alert alert-danger">{{Session::get('error')}}</div>
            @endif
        </div>
    </div>
    <div class="content-main mt-2">
        <div class="row">
           <div class="col-md-12">
                <div class="table-responsive mt-2" >
                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Serial</th>
                            <th scope="col">Mode</th>
                            <th scope="col">Volt Pin</th>
                            <th scope="col">% Pin</th>
                            <th scope="col">Radar1</th>
                            <th scope="col">Radar2</th>
                            <th scope="col">Camera1</th>
                            <th scope="col">Camera2</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td id="serial"></td>
                                <td id="mode"></td>
                                <td id="volt"></td>
                                <td id="competi"></td>
                                <td id="rada1"></td>
                                <td id="rada2"></td>
                                <td id="cam1"></td>
                                <td id="cam2"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

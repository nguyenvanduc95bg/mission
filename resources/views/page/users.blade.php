@extends('layouts.master')
@section('content')
<div class="row">
    <div class="content-header">
        <div class="content-header__logo">
            <h2>Users</h2>
            <span>Create and edit users</span>
            @if(Session::has('success'))
            <div class="alert alert-success">{{Session::get('success')}}</div>
            @endif
            @if(Session::has('error'))
            <div class="alert alert-danger">{{Session::get('error')}}</div>
            @endif
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createUser"><i class="fas fa-plus"></i>Create user</button>
        </div>
    </div>
    <div class="content-main mt-2">
        <div class="row">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header">
                        <label for="">List user</label><br>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive mt-2">
                            <table class="table table-striped table-reponsive" id="dataTables-example">
                                <thead>
                                    <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Role</th>
                                    <td></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1 ?>
                                    @foreach($users as $item)
                                    <tr>
                                        <th scope="row">{{$i++}}</th>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->email}}</td>
                                        <td>
                                            @if($item->level == 999) 
                                            <span class="text-danger">Admin</span>
                                            @else 
                                            <span class="text-primary">User</span>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                <a href="#" class="btn btn-secondary showdetails" data-toggle="modal" data-target="#updateUser" data-id="{{$item->id}}" data-name="{{$item->name}}">
                                                    <i class="mdi mdi-grease-pencil"></i>
                                                </a>
                                                <a href="/delete-users/{{$item->id}}" onclick="return confirm('Are you sure?')" class="btn btn-danger mx-2">
                                                    <i class="mdi mdi-delete"></i>
                                                </a>
                                                <a  href="#" class="btn btn-success ">
                                                    <i class="mdi mdi-note"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
            </div>
        </div>
    </div>
</div>
    <div class="modal fade" id="createUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Create User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
            <form action="/create-user" method="post">
                @csrf
            <div class="modal-body create-body">
                <label for="">Name</label><br>
                <input type="text" name="name" class="form-control" required>
                <label for="">Email</label><br>
                <input type="text" name="email" class="form-control" required>
                <label for="">Mật Khẩu</label><br>
                <input type="text" name="password" class="form-control" required>
                <label for="">Role</label><br>
                <select name="level" id="" class="form-control">
                    <option value="0">User</option>
                    <option value="999">Admin</option>
                </select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success">Create user</button>
            </div>
            </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="updateUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Update User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
            <form action="/update-user" method="post">
                @csrf
                <div class="modal-body create-body">
                    <input type="hidden" name="user_id" class="form-control" id="user_id">
                    <label for="">Name</label><br>
                    <input type="text" name="map_name" class="form-control" id="name" required>
                    <label for="">Mật khẩu </label><br>
                    <input type="text" name="password" class="form-control" id="pointa" required>
                </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success">Update User</button>
            </div>
            </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var elements = document.getElementsByClassName("showdetails");

        var myFunction = function() {
            var id = this.getAttribute("data-id");
            var name = this.getAttribute("data-name");
            document.getElementById('user_id').value=id;
            document.getElementById('name').value=name;
        };

        for (var i = 0; i < elements.length; i++) {
            elements[i].addEventListener('click', myFunction, false);
        }

    </script>
@endsection


@extends('layouts.master')
@section('content')
<div class="row">
    <h2>List Of Function</h2>
    <br>
    <br>
    <br>
    <div class="content">
        <div class="item">
            <a href="/missions" class="btn btn-lg btn-outline-primary btn-icon-text">
                <i class="menu-icon mdi mdi-arrow-right-bold-hexagon-outline"></i><br>
                missions
            </a>
        </div>
        <div class="item">
            <a href="/maps" class="btn btn-lg btn-outline-primary btn-icon-text">
                <i class="menu-icon mdi mdi-google-maps"></i><br>
                Maps
            </a>
        </div>
        <div class="item">
            <a href="/sound" class="btn btn-lg btn-outline-primary btn-icon-text">
                <i class="menu-icon mdi mdi-bookmark-music"></i><br>
                Sound
            </a>
        </div>
        <div class="item">
            <a href="#" class="btn btn-lg btn-outline-primary btn-icon-text">
                <i class="menu-icon mdi mdi-autorenew"></i><br>
                Transition
            </a>
        </div>
        <div class="item">
            <a href="/users" class="btn btn-lg btn-outline-primary btn-icon-text">
                <i class="menu-icon mdi mdi-account-outline"></i><br>
                Users
            </a>
        </div>
        <div class="item">
            <a href="#" class="btn btn-lg btn-outline-primary btn-icon-text">
                <i class="menu-icon mdi mdi-account-multiple"></i><br>
                User groups
            </a>
        </div>
    </div>
</div>
<style>
    .content {
        display: flex;
        justify-content: center;
        flex-wrap: wrap;
    }
    .content .item {
        width: calc(100%/3);
        text-align: center;
        padding: 10px;
    }
    .content .item a {
        width: 100%;
        height: 120px;
        text-transform: uppercase;
    }

    .content .item a i {
        font-size: 30px;
    }

    @media only screen and (max-width: 600px) {
        .content .item {
            width: 100%;
        }
    }
    
    @media only screen and (max-width: 900px) {
        .content .item {
            width: 50%;
        }
    }
</style>
@endsection
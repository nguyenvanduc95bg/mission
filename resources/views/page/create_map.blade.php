<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>MviBot - Smart AMR for logistic</title>
    <style>
      body {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: space-between;
        background: linear-gradient(to right, #0033ff, #009999);
        color: #d7d7ef;
        font-family: "LATO", sans-serif;
      }

      h2 {
        margin: 50px 0;
      }

      .file-drop-area {
        position: relative;
        display: flex;
        align-items: center;
        max-width: 100%;
        padding: 25px;
        border: 1px dashed rgba(255, 255, 255, 0.4);
        border-radius: 3px;
        transition: 0.2s;
      }

      .choose-file-button {
        flex-shrink: 0;
        background-color: rgba(255, 255, 255, 0.04);
        border: 1px solid rgba(255, 255, 255, 0.1);
        border-radius: 3px;
        padding: 8px 15px;
        margin-right: 10px;
        font-size: 12px;
        text-transform: uppercase;
        color: lightblue;
      }

      .file-message {
        font-size: small;
        font-weight: 300;
        line-height: 1.4;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        margin-right: 0.5rem;
      }

      .file-input {
        position: absolute;
        left: 0;
        top: 0;
        height: 100%;
        widows: 100%;
        cursor: pointer;
        opacity: 0;
      }

      #divImageMediaPreview {
        position: relative;
        margin-top: 1rem;
      }
    </style>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="/paint/script.js"></script>
    <div class="top">
      <h2>FILE UPLOAD IMAGE</h2>
      <div class="file-drop-area">
        <span class="choose-file-button">Choose Files</span>
        <span class="file-message">or drag and drop files here</span>
        <input type="file" class="file-input" accept=".jfif,.jpg,.jpeg,.png,.gif,.pgm" />
        <p>
          <label>Drawing tool:</label>
          <select id="dtool" class="choose-file-button">
            <option value="line">Line</option>
            <option value="rect">Rectangle</option>
            <option value="pencil">Pencil</option>
          </select>
          <button id="clear" class="choose-file-button">Clear</button>
        </p>
      </div>
    </div>
    <div id="divImageMediaPreview"></div>
  </head>
</html>

<script>
var scale = 1,
panning = false,
pointX = 0,
pointY = 0,
start = { x: 0, y: 0 },
zoom = document.getElementById("divImageMediaPreview");

function setTransform() {
zoom.style.transform =
    "translate(" + pointX + "px, " + pointY + "px) scale(" + scale + ")";
};

// zoom.onmousedown = function (e) {
//     e.preventDefault();
//     start = { x: e.clientX - pointX, y: e.clientY - pointY };
//     panning = true;
// };

// zoom.onmouseup = function (e) {
//     panning = false;
// };

// zoom.onmousemove = function (e) {
//     e.preventDefault();
//     if (!panning) {
//         return;
//     }
//     pointX = e.clientX - start.x;
//     pointY = e.clientY - start.y;
//     setTransform();
// };

zoom.onwheel = function (e) {
    e.preventDefault();
    var xs = (e.clientX - pointX) / scale,
        ys = (e.clientY - pointY) / scale,
        delta = e.wheelDelta ? e.wheelDelta : -e.deltaY;
    delta > 0 ? (scale *= 1.2) : (scale /= 1.2);
    pointX = e.clientX - xs * scale;
    pointY = e.clientY - ys * scale;
    setTransform();
};
</script>
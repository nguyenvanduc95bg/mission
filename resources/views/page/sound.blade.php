@extends('layouts.master')
@section('content')
<div class="rows">
    <div class="content-header">
        <div class="content-header__logo">
            <h2>Sounds</h2>
            @if(Session::has('success'))
            <div class="alert alert-success">{{Session::get('success')}}</div>
            @endif
            @if(Session::has('error'))
            <div class="alert alert-danger">{{Session::get('error')}}</div>
            @endif
        </div>
    </div>
    <div class="content-main mt-2">
        <div class="row">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header">
                        <label for="">Show sounds</label><br>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive mt-2">
                            <table class="table table-striped table-reponsive" id="dataTables-example">
                                <thead>
                                    <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th>File</th>
                                    <td></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1 ?>
                                    @foreach($sounds as $item)
                                    <tr>
                                        <th scope="row">{{$i++}}</th>
                                        <td>
                                            {{$item->name}}
                                        </td>
                                        <td>
                                            <audio controls>
                                                <source src="{{$item->path}}" type="audio/mpeg">
                                            </audio>
                                        </td>
                                        <td>
                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                <a href="/delete-sound/{{$item->id}}" onclick="return confirm('Are you sure?')" class="btn btn-danger mx-2">
                                                    <i class="mdi mdi-delete"></i>
                                                </a>
                                                <a  href="/showSound/{{$item->id}}" class="btn btn-success ">
                                                    <i class="mdi mdi-note"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <form action="/create-sound" method="POST" enctype='multipart/form-data'>
                    @csrf
                    <input type="file" required name="sound_file" class="form-control" accept=".mp3">
                    <label for="" class="text-danger">* Choose file mp3 to import data</label><br>
                    <input type="submit" class="btn btn-primary mt-2" value="Import sound"/>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.master')
@section('content')
<div class="row">
    <div class="content-header">
        <div class="content-header__logo">
            <h2>Add point</h2>
            @if(Session::has('success'))
            <div class="alert alert-success">{{Session::get('success')}}</div>
            @endif
            @if(Session::has('error'))
            <div class="alert alert-danger">{{Session::get('error')}}</div>
            @endif
            
            <a href="/missions" class="btn btn-primary">Back</a>
        </div>
    </div>
    <div class="content-main mt-2">
        <div class="row">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header">
                        <label for="">List point</label><br>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-reponsive" id="dataTables-example">
                            <thead>
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Point</th>
                                <th scope="col">Coordinate</th>
                                <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1 ?>
                                @foreach($toados as $item)
                                <tr>
                                    <th scope="row">{{$i++}}</th>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->toado}}</td>
                                    <td>
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            <a href="/delete-point/{{$item->id}}" onclick="return confirm('Are you sure?')" class="btn btn-danger mx-2">
                                                <i class="mdi mdi-delete"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header">
                        <label for="">Add point</label><br>
                    </div>
                    <div class="card-body">
                        <form action="/addpoint" method="post">
                            @csrf
                            <label for="">Group</label><br>
                            <select class="form-control" name="group" id="">
                                @foreach($groups as $data)
                                    <option value="{{$data->id}}">{{$data->name}}</option>
                                @endforeach
                            </select>
                            <label for="">Point</label><br>
                            <input type="text" class="form-control" id="name" name="name" required>
                            <label for="">Coordinate 1 </label><br>
                            <input type="text" class="form-control" id="toado1" name="toado1" required>
                            <label for="">Coordinate 2</label><br>
                            <input type="text" class="form-control" id="toado2" name="toado2" required>
                            <label for="">Coordinate 3</label><br>
                            <input type="text" class="form-control" id="toado3" name="toado3" required>
                            <label for="">Coordinate 4</label><br>
                            <input type="text" class="form-control" id="toado4" name="toado4" required>
                            <input type="submit" class="btn btn-primary mt-2">
                            <a href="javascript:void(0)" onclick="left()" class="btn btn-danger mt-2"> Left </a>
                            <a href="javascript:void(0)" onclick="right()" class="btn btn-success mt-2"> Right </a>
                            <a href="javascript:void(0)" onclick="degre90()" class="btn btn-primary mt-2"> 90 degre </a>
                            <a href="javascript:void(0)" onclick="degre180()" class="btn btn-danger mt-2"> 180 degre </a>
                            <a href="javascript:void(0)" onclick="stop()" class="btn btn-success mt-2"> Stop </a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script type="text/javascript">
    function left() {
        document.getElementById('name').value='left'
        document.getElementById('toado1').value=-2
        document.getElementById('toado2').value=-2
        document.getElementById('toado3').value=-1
        document.getElementById('toado4').value=-1
    }
    function right() {
        document.getElementById('name').value='right'
        document.getElementById('toado1').value=-3
        document.getElementById('toado2').value=-3
        document.getElementById('toado3').value=-1
        document.getElementById('toado4').value=-1
    }
    function degre90() {
        document.getElementById('name').value='90 degre'
        document.getElementById('toado1').value=-4
        document.getElementById('toado2').value=-4
        document.getElementById('toado3').value=-1
        document.getElementById('toado4').value=-1
    }
    function degre180() {
        document.getElementById('name').value='180 degre'
        document.getElementById('toado1').value=-5
        document.getElementById('toado2').value=-5
        document.getElementById('toado3').value=-1
        document.getElementById('toado4').value=-1
    }
    function stop() {
        document.getElementById('name').value='wait'
        document.getElementById('toado1').value=-6
        document.getElementById('toado2').value=-6
        document.getElementById('toado3').value=-1
        document.getElementById('toado4').value=-1
    }

</script>

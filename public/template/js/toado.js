var selectedRow = null
function onFormSubmit() {
    if (validate()) {
        var formData = readFormData();
        if (selectedRow == null)
            insertNewRecord(formData);
        else
            updateRecord(formData);
        resetForm();
    }
}
function readFormData() {
    var formData = {};
    formData["Coordinate1"] = document.getElementById("Coordinate1").value;
    formData["Coordinate2"] = document.getElementById("Coordinate2").value;
    formData["Coordinate3"] = document.getElementById("Coordinate3").value;
    formData["Coordinate4"] = document.getElementById("Coordinate4").value;
    return formData;
}
function insertNewRecord(data) {
    var table = document.getElementById("CoordinateList").getElementsByTagName('tbody')[0];
    var newRow = table.insertRow(table.length);
    cell1 = newRow.insertCell(0);
    cell1.innerHTML = data.Coordinate1;
    cell2 = newRow.insertCell(1);
    cell2.innerHTML = data.Coordinate2;
    cell3 = newRow.insertCell(2);
    cell3.innerHTML = data.Coordinate3;
    cell4 = newRow.insertCell(3);
    cell4.innerHTML = data.Coordinate4;
    cell4 = newRow.insertCell(4);
    cell4.innerHTML = `<a onClick="onEdit(this)">Edit</a>
                       <a onClick="onDelete(this)">Delete</a>`;
}
function resetForm() {
    document.getElementById("Coordinate1").value = "";
    document.getElementById("Coordinate2").value = "";
    document.getElementById("Coordinate3").value = "";
    document.getElementById("Coordinate4").value = "";
    selectedRow = null;
}
function onEdit(td) {
    selectedRow = td.parentElement.parentElement;
    document.getElementById("Coordinate1").value = selectedRow.cells[0].innerHTML;
    document.getElementById("Coordinate2").value = selectedRow.cells[1].innerHTML;
    document.getElementById("Coordinate3").value = selectedRow.cells[2].innerHTML;
    document.getElementById("Coordinate4").value = selectedRow.cells[3].innerHTML;
}
function updateRecord(formData) {
    selectedRow.cells[0].innerHTML = formData.Coordinate1;
    selectedRow.cells[1].innerHTML = formData.Coordinate2;
    selectedRow.cells[2].innerHTML = formData.Coordinate3;
    selectedRow.cells[3].innerHTML = formData.Coordinate4;
}
function onDelete(td) {
    if (confirm('Are you sure to delete this record ?')) {
        row = td.parentElement.parentElement;
        document.getElementById("CoordinateList").deleteRow(row.rowIndex);
        resetForm();
    }
}
function validate() {
    isValid = true;
    if (document.getElementById("Coordinate1").value == "") {
        isValid = false;
        document.getElementById("Coordinat1ValidationError").classList.remove("hide");
    } else {
        isValid = true;
        if (!document.getElementById("Coordinate1ValidationError").classList.contains("hide"))
            document.getElementById("Coordinate1ValidationError").classList.add("hide");
    }
    return isValid;
}

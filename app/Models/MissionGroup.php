<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MissionGroup extends Model
{
    use HasFactory;

    protected $table = "mission_groups";
}

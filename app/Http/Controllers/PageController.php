<?php

namespace App\Http\Controllers;

use App\Models\Map;
use App\Models\Mission;
use App\Models\MissionGroup;
use App\Models\SavePoint;
use App\Models\Sound;
use App\Models\Toado;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class PageController extends Controller
{
    public function index() {
        return view('page.index');
    }

    public function listMission() {
        $mission_groups = MissionGroup::orderBy('created_at', 'desc')->get();
        $missions = Mission::orderBy('created_at', 'desc')->get();
        $savepoints = SavePoint::all();
        return view('page.mission', compact('mission_groups', 'missions', 'savepoints'));
    }

    public function filterMission(Request $req) {
        $mission_groups = MissionGroup::orderBy('created_at', 'desc')->get();
        if ($req->filter_group == 0) {
            $missions = Mission::orderBy('created_at', 'desc')->get();
        } else {
            $missions = Mission::where('mission_group_id', $req->filter_group)->orderBy('created_at', 'desc')->get();
        }
        $savepoints = SavePoint::all();
        $id_group = $req->filter_group;
        return view('page.mission', compact('mission_groups', 'missions', 'savepoints', 'id_group'));
    }

    public function createMissionGroup(Request $request)
    {
        $mission_group = new MissionGroup();
        $mission_group->name = $request->group_mission_name;
        $mission_group->save();
        return redirect()->back()->with('success', 'Added Mission_group');
    }

    public function deleteMissonGroup($id)
    {
        $mission_group = MissionGroup::where('id', $id)->first();
        $mission_group->delete();
        return redirect()->back()->with('success', 'Deleted Mission_group');
    }

    public function createMission(Request $req)
    {
        $misson = new Mission();
        $misson->name = $req->mission_name;
        $misson->description = $req->mission_description;
        $misson->type = $req->type;
        $misson->mission_group_id = $req->group;
        $misson->toado_id = $req->toado;
        $misson->save();
        return redirect()->back()->with('success', 'Added Mission');
    }

    public function deleteMisson($id)
    {
        $mission = Mission::where('id', $id)->first();
        $mission->delete();
        return redirect()->back()->with('success', 'Deleted Mission');
    }

    public function updateMission(Request $req)
    {
        $misson = Mission::where('id', $req->mission_id)->first();
        $misson->name = $req->mission_name;
        $misson->description = $req->mission_description;
        $misson->type = $req->type;
        $misson->mission_group_id = $req->group;
        $misson->save();
        return redirect()->back()->with('success', 'Updated Mission');
    }

    public function listMap() {
        //update maps in folder
        $path = public_path('file_maps');
        $files = \File::allFiles($path);
        $fileNames = [];
        foreach($files as $file) {
            array_push($fileNames, pathinfo($file)['filename']);
        }
        foreach($fileNames as $name) {
            Map::updateOrCreate(['name' => $name], [
                'name' => $name,
                'pointA' => 0,
                'pointB' => 0,
                'pointC' => 0,
                'pointD' => 0,
            ]);
        }
        $maps = Map::orderBy('created_at', 'desc')->get();
        return view('page.maps', compact('maps'));
    }

    public function createMap(Request $req)
    {
        $mapExists = Map::where('name', $req->map_name)->first();
        if($mapExists) {
            return redirect()->back()->with('error', ' This Map Have Already Exist ');
        }
        $map = new Map();
        $map->name = $req->map_name;
        $map->pointA = $req->pointa;
        $map->pointB = $req->pointb;
        $map->pointC = $req->pointc;
        $map->pointD = $req->pointd;
        $map->save();
        return redirect()->back()->with('success', 'Added Map');
    }

    public function deleteMaps($id)
    {
        $map = Map::where('id', $id)->first();
        $path = "file_maps/" . $map->name . ".yaml";
        if (\File::exists($path)) {
            unlink($path);
        }
        $map->delete();
        return redirect()->back()->with('success', 'Deleted Map');
    }

    public function updateMap(Request $req)
    {
        $map = Map::where('id', $req->map_id)->first();
        $map->name = $req->map_name;
        $map->pointA = $req->pointa;
        $map->pointB = $req->pointb;
        $map->pointC = $req->pointc;
        $map->pointD = $req->pointd;
        $map->save();
        return redirect()->back()->with('success', 'Updated Map');
    }

    public function importMap(Request $req)
    {
        $fileCSV = $req->file('import_file');
        $name = explode('.',$fileCSV->getClientOriginalName())[0];

        $map = Map::updateOrCreate(['name' => $name], [
            'name' => $name,
            'pointA' => 0,
            'pointB' => 0,
            'pointC' => 0,
            'pointD' => 0,
        ]);

        // $handle = fopen($fileCSV,"r");
        // // count row number 
        // $row = 0;
        // // add you row number for skip 
        // // hear we pass 1st row for skip in csv 
        // $skip_row_number = array("1");
        // while (($data = fgetcsv($handle)) !== FALSE) {
        //     // if( $row == 1) 
        //     // { 
        //     //     continue;
        //     // }
        //     // $row++;
        //     $row++;	
        //     // check row for skip row 	
        //     if (in_array($row, $skip_row_number))	
        //     {
        //         continue; 
        //         // skip row of csv
        //     } else {
        //         $map = [
        //             'name' => $data[0],
        //             'pointA' => $data[1],
        //             'pointB' => $data[2],
        //             'pointC' => $data[3],
        //             'pointD' => $data[4],
        //         ];
        //         $validator = Validator::make($map, [
        //             "name" => "required",
        //             "pointA" => "required",
        //             "pointB" => "required",
        //             "pointC" => "required",
        //             "pointD" => "required",
        //         ]);
        //         if ($validator->fails()) {
        //             continue;
        //         }
        //         $map = Map::where('name', $data[0]);
        //         $map = Map::updateOrCreate(['name' => $data[0]], [
        //             'name' => $data[0],
        //             'pointA' => $data[1],
        //             'pointB' => $data[2],
        //             'pointC' => $data[3],
        //             'pointD' => $data[4],
        //         ]);
        //     }
        // }
        return redirect()->back()->with('success', 'Imported Data');
    }

    public function exportMap(Request $request)
    {
        $fileName = 'maps.csv';
        $tasks = Map::all();

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = array('name', 'pointA', 'pointB', 'pointC', 'pointD');

        $callback = function() use($tasks, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($tasks as $task) {
                $row['name']  = $task->name;
                $row['pointA']  = $task->pointA;
                $row['pointB']  = $task->pointB;
                $row['pointC']  = $task->pointC;
                $row['pointD']  = $task->pointD;

                fputcsv($file, array($row['name'], $row['pointA'], $row['pointB'], $row['pointC'], $row['pointD']));
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }

    public function getCreateMap()
    {
        return view('page.create_map');
    }

    public function getLogin(){
    	return view('page.login');
    }

    public function postLogin(Request $request){	
    	   $this->validate($request,
            [
                'email' => 'required',
                'password' => 'required|min:6',
                
            ],
            [
                'email.required' => 'Please Enter Your Email',
                'password.required' => 'Please Enter Your Password',
                'password.min' => 'Password have must be least 6 characters',
            ]
        );
        
        $user = array('email'=>$request->email, 'password'=>$request->password);
        if(Auth::attempt($user)){
            return redirect('/');
        }
        else{
            return redirect('login')->with('error','Email or Password is incorrect');
        }
    }

    public function getLogout(){
        Auth::logout();
        return redirect('/');
    }

    public function listUser() {
        $users = User::where('id', '<>', Auth::user()->id)->orderBy('created_at', 'desc')->get();
        return view('page.users', compact('users'));
    }

    public function createUser(Request $req)
    {
        $userExits = User::where('email', $req->email)->first();
        if($userExits) {
            return redirect()->back()->with('error', 'This User Have Already Exist');
        }
        $user = new User();
        $user->name = $req->name;
        $user->email = $req->email;
        $user->password = Hash::make($req->password);
        $user->level = $req->level;
        $user->save();
        return redirect()->back()->with('success', 'Added User');
    }

    public function deleteUser($id)
    {
        $user = User::where('id', $id)->first();
        $user->delete();
        return redirect()->back()->with('success', 'Deleted User');
    }

    public function updateUser(Request $req)
    {
        $user = User::where('id', $req->id)->first();
        $user->name = $req->name;
        if ($req->password) {
            $user->password = Hash::make($req->password);
        }
        $user->save();
        return redirect()->back()->with('success', 'Updated');
    }

    public function addPoint()
    {
        $groups = MissionGroup::orderBy('id', 'desc')->get();
        $toados = Toado::orderBy('id', 'desc')->get();
        return view('page.addpoint', compact('groups', 'toados'));
    }

    public function postaddPoint(Request $req) 
    {
        // $toado = Toado::where('mission_group_id', $req->group)->first();
        // if ($toado) {
        //     $toado->name = $req->name;
        //     $toado->mission_group_id = $req->group;
        //     $toado->toado = "(".$req->toado1 . '|' . $req->toado2 . '|' . $req->toado3 . '|' . $req->toado4 . ')';
        //     $toado->save();
        // } else {
           
        // }
        $toado = new Toado();
        $toado->name = $req->name;
        $toado->mission_group_id = $req->group;
        $toado->toado = "(".$req->toado1 . '|' . $req->toado2 . '|' . $req->toado3 . '|' . $req->toado4 . ')';
        $toado->save();
        return redirect()->back()->with('success', 'Added');
    }

    public function deletePoint($id) 
    {
        $toa = Toado::where('id', $id)->first();
        $toa->delete();
        return redirect()->back()->with('success', 'Deleted');
    }

    public function runMission($id) 
    {
        $mission = Mission::where('id', $id)->with('toados')->first();
        return response()->json($mission, 200);
    }

    public function showMap($id) 
    {
        $map = Map::where('id', $id)->first();
        return response()->json($map, 200);
    }

    public function getRunMission($id)
    {
        $mission = Mission::where('id', $id)->with('toados')->first();
        return view('page.run_mission', compact('mission'));
    }

    public function postRunMission(Request $req)
    {
        $mission = Mission::where('id', $req->run_mission_id)->with('toados')->first();
        // foreach($mission->toados as $toado) {
        //     $toado->toado = $req->unit . $toado->toado;
        // }
        $mission->unit = $req->unit;
        return view('page.run_mission', compact('mission'));
    }

    public function getShowMap($id)
    {
        $map = Map::where('id', $id)->first();
        return view('page.show_map', compact('map'));
    }

    public function getAddPointByValue($id, $value) {
        if ($value == 'left') {
            $toado = new Toado();
            $toado->name = 'Left';
            $toado->toado = '(-2|-2|-1|-1)';
            $toado->mission_id = $id;
            $toado->save();
        }
        if ($value == 'right') {
            $toado = new Toado();
            $toado->name = 'Right';
            $toado->toado = '(-3|-3|-1|-1)';
            $toado->mission_id = $id;
            $toado->save();
        }
       
        if ($value == '90') {
            $toado = new Toado();
            $toado->name = '90 degre';
            $toado->toado = '(-4|-4|-1|-1)';
            $toado->mission_id = $id;
            $toado->save();
        }

        if ($value == '180') {
            $toado = new Toado();
            $toado->name = '180 degre';
            $toado->toado = '(-5|-5|-1|-1)';
            $toado->mission_id = $id;
            $toado->save();
        }

        if ($value == 'wait') {
            $toado = new Toado();
            $toado->name = 'Wait';
            $toado->toado = '(-6|-6|-1|-1)';
            $toado->mission_id = $id;
            $toado->save();
        }
        if ($value == 'go ahead') {
            $toado = new Toado();
            $toado->name = 'Go Ahead';
            $toado->toado = '(-7|-7|-1|-1)';
            $toado->mission_id = $id;
            $toado->save();
        }
        return redirect()->back()->with('success', 'Added');
    }

    public function getSavePointByValue($name, $toado) {
        $savepoint = new SavePoint();
        $savepoint->name = $name;
        $savepoint->toado = $toado;
        $savepoint->save();
        return redirect('/missions');
    }

    public function deleteSavePoint($id) {
        $savepoint = SavePoint::where('id', $id)->first();
        $savepoint->delete();
        return redirect()->back()->with('message', 'Đã xóa');
    }

    public function runSavePoint($id)
    {
        $savepoint = SavePoint::where('id', $id)->first();
        return view('page.run_savepoint', compact('savepoint'));
    }

    public function listSound() {
        //update maps in folder
        $path = 'file_sounds';
        $files = \File::allFiles($path);
        $fileNames = [];
        foreach($files as $file) {
            array_push($fileNames, pathinfo($file)['filename']);
        }
        foreach($fileNames as $name) {
            $path = 'file_sounds' . '/' . $name . ".mp3";
            Sound::updateOrCreate(['name' => $name], [
                'name' => $name,
                'path' => $path,
            ]);
        }
        $sounds = Sound::orderBy('created_at', 'desc')->get();
        return view('page.sound', compact('sounds'));
    }

    public function createSound(Request $req)
    {
        $file = $req->file('sound_file');
        $name = pathinfo($file->getClientOriginalName())['filename'];
        $soundExists = Sound::where('name', $name)->first();
        if($soundExists) {
            return redirect()->back()->with('error', ' This Sound Have Already Exist ');
        }
        $sound = new Sound();
        $sound->name = $name;
        $file->move('file_sounds', $file->getClientOriginalName());  
        $sound->path = 'file_sounds/' . $file->getClientOriginalName();
        $sound->save();
        return redirect()->back()->with('success', 'Added Sound');
    }

    public function deleteSound($id)
    {
        $sound = Sound::where('id', $id)->first();
        if (\File::exists($sound->path)) {
            unlink($sound->path);
        }
        $sound->delete();
        return redirect()->back()->with('success', 'Deleted Sound');
    }

    public function getShowSound($id)
    {
        $sound = Sound::where('id', $id)->first();
        return view('page.show_sound', compact('sound'));
    }

    public function status() {
        return view('page.status');
    }

    public function getToadoByGroup(Request $req) {
        $toados = Toado::where('mission_group_id', $req->group_id)->get();
        return response()->json([
			'status' => 200,
			'data' => $toados,
		]);
    }

}
<?php

use App\Http\Controllers\PageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['userLogin']], function() { 
	Route::get('/', [PageController::class, 'index']);
	Route::get('/missions', [PageController::class, 'listMission']);
	Route::post('/create-mission-groups', [PageController::class, 'createMissionGroup']);
	Route::get('/delete-misson-group/{id}', [PageController::class, 'deleteMissonGroup']);
	Route::post('/create-mission', [PageController::class, 'createMission']);
	Route::get('/delete-mission/{id}', [PageController::class, 'deleteMisson']);
	Route::post('/update-mission', [PageController::class, 'updateMission']);
	Route::get('/maps', [PageController::class, 'listMap']);
	Route::post('/create-map', [PageController::class, 'createMap']);
	Route::get('/delete-maps/{id}', [PageController::class, 'deleteMaps']);
	Route::post('/update-map', [PageController::class, 'updateMap']);
	Route::post('/import-map', [PageController::class, 'importMap']);
	Route::get('/export-maps', [PageController::class, 'exportMap']);
	Route::get('/create-map', [PageController::class, 'getCreateMap']);
	Route::get('/users', [PageController::class, 'listUser']);
	Route::post('/create-user', [PageController::class, 'createUser']);
	Route::get('/delete-users/{id}', [PageController::class, 'deleteUser']);
	Route::post('/update-user', [PageController::class, 'updateUser']);
	Route::get('/add-point', [PageController::class, 'addPoint']);
	Route::post('/addpoint', [PageController::class, 'postaddPoint']);
	Route::get('delete-point/{id}', [PageController::class, 'deletePoint']);
	Route::get('runMission/{id}', [PageController::class, 'getRunMission']);
	Route::post('run-mission', [PageController::class, 'postRunMission']);
	Route::get('showMap/{id}', [PageController::class, 'getshowMap']);
	Route::get('add_point/{id}/{value}', [PageController::class, 'getAddPointByValue']);
	Route::get('save-point/{name}/{toado}', [PageController::class, 'getSavePointByValue'])->name('save-point');
	Route::get('/delete-savepoint/{id}', [PageController::class, 'deleteSavePoint']);
	Route::get('/runSavePoint/{id}', [PageController::class, 'runSavePoint']);

	Route::get('/sound', [PageController::class, 'listSound']);
	Route::post('/create-sound', [PageController::class, 'createSound']);
	Route::get('/delete-sound/{id}', [PageController::class, 'deleteSound']);
	Route::get('showSound/{id}', [PageController::class, 'getshowSound']);
	Route::get('/status', [PageController::class, 'status']);
	Route::get('/filter-mission', [PageController::class, 'filterMission']);
	Route::get('/getToadoByGroup', [PageController::class, 'getToadoByGroup']);
});
//Login
Route::get('login', [PageController::class, 'getLogin']);
Route::post('login', [PageController::class, 'postLogin']);

//Logout
Route::get('logout', [PageController::class, 'getLogout']);

//controlrobot
Route::get('/control-robot/index', function() {
	return view('controlRobot.index');
});

Route::get('/control-robot/index1', function() {
	return view('controlRobot.index1');
});

Route::get('/control-robot/index2', function() {
	return view('controlRobot.index2');
});

Route::get('/control-robot/index3', function() {
	return view('controlRobot.index3');
});

Route::get('/control-robot/index4', function() {
	return view('controlRobot.index4');
});

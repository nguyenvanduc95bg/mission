/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : mission

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2022-01-05 22:31:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int(11) NOT NULL COMMENT '0: user, 999: admin',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Admin', 'admin@gmail.com', null, '$2y$10$qiw/KiIgioegotPq2nzOreQYDi6O5CEaUSS.cgUkZoDW25d/Tw8iu', '999', null, null, null);
INSERT INTO `users` VALUES ('2', 'User 1', 'user1@gmail.com', null, '$2y$10$zzSygdYgHYK1iVVgQCxAl.FnuyQ911Uq.4QInyjHBx6zsWXETIuAG', '0', null, '2022-01-05 15:30:55', '2022-01-05 15:30:55');
INSERT INTO `users` VALUES ('3', 'admin1', 'admin1@gmail.com', null, '$2y$10$Ko7r9k8j3j/4WrQbv6k6De.8WF6ZKLsaL1bRFX65An519KIjRZYPK', '999', null, '2022-01-05 15:31:08', '2022-01-05 15:31:08');
